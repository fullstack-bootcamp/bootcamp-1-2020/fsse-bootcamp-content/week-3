# Week 3 - Intro to Frontend

We have created four subsystems up to this point:

1. Python HTTP Client (`client`)
2. Node.js Backend (`cloud`)
3. Python SUMO vehicle simulation (`embedded`)
4. Frontend Client (`fontend-basic` or `frontend-react`)

## Setup steps to run system:

1. Start backend on port 9500

- `cd cloud`
- `node index.js`

2. Run python HTTP client to provision three vehicles

- `cd client`
- `source venv/bin/activate`
- `python main.py`

3. Start frontend server on port 3000 and open in browser

- `cd frontend-react`
- `yarn start`
- Open http://localhost:3000 in your browser

4. Run SUMO vehicle simulation

- `cd embedded`
- `source venv/bin/activate`
- `python vehicle.py`
