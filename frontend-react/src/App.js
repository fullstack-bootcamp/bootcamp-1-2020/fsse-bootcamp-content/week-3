import React from "react";
// import "./App.css";

import { Switch, Route, BrowserRouter } from "react-router-dom";

import Navbar from "./lib/navbar/Navbar";
import Home from "./screens/home/Home";
import Vehicles from "./screens/vehicles/Vehicles";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <h1>Fullstack Systems Engineering Viewer</h1>
        <Navbar />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/home" exact component={Home} />
          <Route path="/vehicles" exact component={Vehicles} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
