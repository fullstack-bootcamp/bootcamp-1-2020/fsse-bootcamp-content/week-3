const apiBaseUrl = "http://localhost:9500";

// send an HTTP GET request to API
export const get = ({ route }) => {
  const url = `${apiBaseUrl}${route}`;

  return fetch(url, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  }).then((res) => {
    return res.json();
  });
};
