import React from "react";

import * as api from "../../utils/api";

class Vehicles extends React.Component {
  state = {
    vehicleList: [],
    vehicleData: {},
    intervalHandles: [],
  };

  componentDidMount() {
    api
      .get({ route: "/vehicles" })
      .then((jsonData) => {
        this.setState({ vehicleList: jsonData });
        return jsonData;
      })
      .then((vehicleList) => {
        // setup polling for latest data for each vehicle
        vehicleList.map((vehicle) => {
          const intervalHandle = setInterval(() => {
            const route = `/vehicles/${vehicle.id}/latest-data`;
            api
              .get({ route })
              .then((latestData) => {
                const oldData = this.state.vehicleData;
                this.setState({
                  vehicleData: { ...oldData, [vehicle.id]: latestData },
                });
              })
              .catch((err) => {
                console.log("GET request to " + route + " failed.");
                console.log({ error: err });
              });
          }, 1000);

          // add this intervalHandle to the component state
          return this.setState({
            intervalHandles: [...this.state.intervalHandles, intervalHandle],
          });
        });
      })
      .catch((err) => console.log(err));
  }

  componentWillUnmount() {
    this.state.intervalHandles.map((intervalHandle) => {
      // call Web API method `clearInterval` to prevent memory leak
      return clearInterval(intervalHandle);
    });
  }

  render() {
    const { vehicleList, vehicleData } = this.state;

    const vehicleCardList =
      vehicleList && vehicleList.length ? (
        vehicleList.map((vehicle) => {
          const vehicleHeading = `${vehicle.year} - ${vehicle.make} ${vehicle.model}`;

          return (
            <div key={vehicle.id}>
              <h3>{vehicleHeading}</h3>
              <ul>
                <li>id: {vehicle.id}</li>
                <li>name: {vehicle.name}</li>
                <li>color: {vehicle.color}</li>
                <li>
                  latest data:{" "}
                  {vehicleData[vehicle.id]
                    ? JSON.stringify(vehicleData[vehicle.id])
                    : "..."}
                </li>
              </ul>
              <img src={vehicle.imageUrl} alt={vehicleHeading} />
            </div>
          );
        })
      ) : (
        <p>No vehicles found yet...</p>
      );

    return (
      <div>
        <h2>Vehicles</h2>
        {vehicleCardList}
      </div>
    );
  }
}

export default Vehicles;
