import React from "react";

class Home extends React.Component {
  render() {
    return (
      <div>
        <h2>Welcome Home</h2>
        <p>This is viewer site for the FSSE vehicle simulator</p>
      </div>
    );
  }
}

export default Home;
