import requests

vehicle_list = [
  {
    "vin": "1ZVHT88S575343170",
    "name": "Dearborn",
    "make": "Ford",
    "model": "Focus",
    "year": 2020,
    "color": "blue",
    "colorRgb": { "r": 0, "g": 0, "b": 255 },
    "imageUrl":
      "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/ford_focus_2020_01.jpg?itok=jGUYRsx3",
  },
  {
    "vin": "1N6BA06A89N316524",
    "name": "Tokyo",
    "make": "Honda",
    "model": "Civic",
    "year": 2020,
    "color": "red",
    "colorRgb": { "r": 255, "g": 0, "b": 0 },
    "imageUrl":
      "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/2020-honda-civic-si-sedan-hpt-386-1591293383.jpg?crop=0.545xw:0.613xh;0.255xw,0.387xh&resize=640:*",
  },
  {
    "vin": "WVWPD63B03P238848",
    "name": "Detroit",
    "make": "Chevrolet",
    "model": "Bolt",
    "year": 2020,
    "color": "white",
    "colorRgb": { "r": 255, "g": 255, "b": 255 },
    "imageUrl":
      "https://cnet3.cbsistatic.com/img/EGXGl4_Ooh9sQ_TM_jVXkWD8hZ0=/2019/10/11/8862140c-2fb0-4be7-aa11-5b44a559cf44/2020-chevrolet-bolt-ogi-02.jpg",
  }
]

# run health check
r = requests.get("http://localhost:9500/health-check")
health_check = r.json()
print("health check result:")
print(health_check["status"])

for vehicle in vehicle_list:
    vin = vehicle["vin"]
    print(f"Provisioning vehicle {vin}")
    p = requests.post(f"http://localhost:9500/provision/{vin}", json=vehicle)
    print(p.json())