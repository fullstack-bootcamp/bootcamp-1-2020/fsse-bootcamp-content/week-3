from time import time, sleep
import os
import sys
import requests

# Mac view homebrew: export SUMO_HOME=/usr/local/Cellar/sumo/1.6.0/
tools = os.path.join(os.environ['SUMO_HOME'], 'share/sumo/tools')
sys.path.append(tools)

import traci

sumoBinary = "sumo-gui"
sumoCmd = [sumoBinary, "-c", "first_network.sumocfg",
           "--start", "--time-to-teleport", "100000000"]


# Vehicle comes off manufacturing line
# VIN = "sumo_car"
# veh = requests.post("http://localhost:9500/provision/"+VIN)
# 
# # save the assigned ID (uuid)
# vehicle = veh.json()
# print(vehicle)
# vehicle_id = vehicle["vehicleId"]

traci.start(sumoCmd)

# Get the vehicle list from the backend
res = requests.get("http://localhost:9500/vehicles")
vehicle_list = res.json()

for vehicle in vehicle_list:
    print("Got vehicle {0} information from the cloud".format(vehicle["name"]))
    traci.vehicle.add(vehicle["id"], "route1")
    color = (vehicle["colorRgb"]["r"], vehicle["colorRgb"]["g"], vehicle["colorRgb"]["b"])
    traci.vehicle.setColor(vehicle["id"], color)
    print("Initial speed: ", traci.vehicle.getSpeed(vehicle["id"]))


step = 1
while step < 10000:
    traci.simulationStep()

    for vehicle in vehicle_list:

        vehicle_id = vehicle["id"]

        try:
            speed = traci.vehicle.getSpeed(vehicle_id)
            accel = traci.vehicle.getAcceleration(vehicle_id)
            distance = traci.vehicle.getDistance(vehicle_id)
            posX, posY = traci.vehicle.getPosition(vehicle_id)
        except:
            print("Vehicle has left the simulation")
            break

        print("Vehicle {0} speed: {1}".format(vehicle_id, speed))

        out = {
            "timestamp": int(time()),
            "vehicleSpeed": speed,
            "acceleration": accel,
            "distance": distance,
            "positionX": posX,
            "positionY": posY
        }
        
        requests.post(f"http://localhost:9500/vehicles/{vehicle_id}/data", json=out)

        current_road = traci.vehicle.getRoadID(vehicle_id)
        if current_road == "top_east":
            traci.vehicle.setStop(vehicle_id, "east_south", duration=10)

    step += 1
    sleep(0.5)

print("done!")
    