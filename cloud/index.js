const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const uuid = require("uuid");
const cors = require("cors");

const PORT = 9500;

const app = express();

app.use(morgan("tiny"));
app.use(bodyParser.json());
app.use(cors());

let vehicles = {};

app.post("/provision/:vin", (req, res) => {
  let id = uuid.v4();
  vehicles[id] = { ...req.body, vin: req.params.vin, data: [] };
  res.json({ vehicleId: id });
});

app.get("/vehicles/:vehicleId", (req, res) => {
  res.json(vehicles[req.params.vehicleId]);
});

app.get("/vehicles", (req, res) => {
  const vehiclesList = Object.keys(vehicles).map((key) => {
    return {
      ...vehicles[key],
      id: key,
    };
  });

  res.json(vehiclesList);
});

app.post("/vehicles/:vehicleId/data", (req, res) => {
  vehicles[req.params.vehicleId].data.push(req.body);
  console.log(vehicles[req.params.vehicleId].data);
  res.json();
});

app.get("/vehicles/:vehicleId/latest-data", (req, res) => {
  const allData = vehicles[req.params.vehicleId].data;

  if (allData.length === 0) {
    return res.json({ status: "No data available" });
  }

  const latestData = allData[allData.length - 1];

  res.json(latestData);
});

app.get("/health-check", (req, res) => {
  res.json({ status: "OK!" });
});

app.listen(PORT, () => {
  console.log(`Cloud listening on ${PORT}`);
});
