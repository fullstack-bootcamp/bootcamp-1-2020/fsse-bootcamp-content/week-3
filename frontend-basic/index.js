const express = require("express");
const path = require("path");

const app = express();

const frontendDir = "/public";

app.use(express.static(path.resolve(__dirname + frontendDir)));

app.get("*", (req, res) => {
  res.sendFile(path.resolve(__dirname + frontendDir + "/index.html"));
});

app.listen(3000, () => {
  console.log("The frontend is being served up on port 3000");
});
