const apiBaseUrl = "http://localhost:9500";

function loadPage(url, urlPathName) {
  const contentElement = document.getElementById("content");
  return fetch(url)
    .then((response) => {
      return response.text();
    })
    .then((data) => {
      contentElement.innerHTML = data;
      window.history.pushState("", "", `/${urlPathName}`);
      return data;
    })
    .catch((err) => {
      console.log("Failed to fetch");
      console.log({ error: err });
    });
}

function apiGet(route) {
  return fetch(`${apiBaseUrl}${route}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  }).then((response) => {
    return response.json();
  });
}

function main() {
  const baseUrl = window.location.origin;
  loadPage(`${baseUrl}/home.html`, "home");

  // home menu item
  const homeLinkElement = document.getElementById("home-nav-link");
  homeLinkElement.addEventListener("click", (e) => {
    e.preventDefault();
    const url = `${baseUrl}${homeLinkElement.pathname}`;
    loadPage(url, "home");
  });

  // vehicles menu item
  const vehiclesLinkElement = document.getElementById("vehicles-nav-link");
  vehiclesLinkElement.addEventListener("click", (e) => {
    e.preventDefault();
    const url = `${baseUrl}${vehiclesLinkElement.pathname}`;
    loadPage(url, "vehicles")
      .then(() => {
        return apiGet("/vehicles");
      })
      .then((vehicleList) => {
        const vehiclesContainer = document.getElementById("vehicles-container");
        vehicleList.forEach((vehicle) => {
          // header
          const header = document.createElement("h3");
          header.textContent = `${vehicle.year} - ${vehicle.make} ${vehicle.model}`;
          vehiclesContainer.appendChild(header);

          // content
          const list = document.createElement("ul");
          vehiclesContainer.appendChild(list);
          let item;
          item = document.createElement("li");
          item.textContent = `id: ${vehicle.id}`;
          list.appendChild(item);

          item = document.createElement("li");
          item.textContent = `name: ${vehicle.name}`;
          list.appendChild(item);

          item = document.createElement("li");
          item.textContent = `color: ${vehicle.color}`;
          list.appendChild(item);

          let latestDataItem = document.createElement("li");
          latestDataItem.textContent = "latest data: ...";
          list.appendChild(latestDataItem);

          // image
          const img = document.createElement("img");
          img.alt = header.textContent;
          img.src = vehicle.imageUrl;
          vehiclesContainer.appendChild(img);

          // get latest data for THIS vehicle
          setInterval(() => {
            apiGet(`/vehicles/${vehicle.id}/latest-data`)
              .then((latestData) => {
                latestDataItem.textContent = `latest data: ${JSON.stringify(
                  latestData
                )}`;
              })
              .catch((err) => console.log(err));
          }, 1000);
        });
      });
  });
}

if (document.readyState === "complete") {
  main();
} else {
  document.addEventListener("DOMContentLoaded", main);
}
